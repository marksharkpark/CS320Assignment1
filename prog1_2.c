#include "prog1_2.h" 
#include <stdio.h> 
#include <stdlib.h> 
//#include <assert.h>
#include <string.h> 

STACK* MakeStack(int initialCapacity){
    STACK* newstack = malloc(sizeof(STACK)); 
    newstack->size = 0; //top? // -1 = stack is empty
    newstack->capacity = initialCapacity; //assigns initialcapacity for array
    newstack->data = (int *) malloc(initialCapacity * sizeof(int)); //Allocate memory for array 
    return newstack; //returns stack pointer;
}

void Push(STACK *newstack,int data){ // Push, PTR & int with data
    if(newstack->size == newstack->capacity) Grow(newstack);
    (*newstack).data[newstack->size]=data;
    (*newstack).size++;    
}

int Pop(STACK *newstack){          // Pop, PTR
    if((*newstack).size == 0) return -1;
    (*newstack).size--;
    return newstack->data[(*newstack).size];
}

void Grow(STACK *newstack){ 
    (*newstack).capacity = (*newstack).capacity *2;
    (*newstack).data = realloc(newstack->data,newstack->capacity * sizeof(int)); 
}
    