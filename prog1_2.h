//#include MAXSIZE 256

typedef struct stack{
    int* data;          //pointer in array
    int  size;          //
    int  capacity;      //
}   STACK;

STACK* MakeStack(int initialCapacity); // Stack Capacity 
void   Push(STACK *stackPtr,int data); // Push, PTR & int with data
int    Pop(STACK *stackPtr);           // Pop, PTR
void   Grow(STACK *stackPtr);          // Grow, PTR
